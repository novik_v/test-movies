import { Injectable } from '@angular/core';

import { GenreType } from './../../../../../content/movie.model';
import { genreType } from './../../../shared/models/genre-type.model';

@Injectable({
    providedIn: 'root',
})
export class GenresService {
    private _genres: GenreType[];

    constructor() {
        this._genres = Object.values(genreType);
    }

    getGenres() {
        return this._genres;
    }
}
