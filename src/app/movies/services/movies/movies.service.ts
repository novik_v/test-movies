import { Injectable } from '@angular/core';
import { of } from 'rxjs';

import { Movie } from '../../../shared/models/movie.model';
import { movies } from '../../../shared/mock/movies.mock';

@Injectable({
    providedIn: 'root',
})
export class MoviesService {
    private _movies: Movie[] = new Array();
    private _query: string;
    private _genre: string;

    constructor() {
        this._movies = movies;
    }

    getMovie(id: number) {
        return of(this._movies.find((movie: Movie) => movie.id === id));
    }

    getMovies({ query, genre }) {
        this._query = query;
        this._genre = genre;

        const refinedQuery = this._query && this._query.toLowerCase();
        const refinedGenre = this._genre && this._genre.toLowerCase();

        return of(
            this._movies.filter(
                (movie: Movie) =>
                    (!query ||
                        movie.name.toLowerCase().includes(refinedQuery)) &&
                    (!genre ||
                        (movie.genres &&
                            movie.genres.some(
                                g => g.toLowerCase() === refinedGenre,
                            ))),
            ),
        );
    }
}
