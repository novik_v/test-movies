import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DetailsComponent } from './containers/details/details.component';
import { MoviesComponent } from './containers/movies/movies.component';

const routes: Routes = [
    { path: 'movies', component: MoviesComponent },
    { path: 'movies/:id', component: DetailsComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})
export class MoviesRoutingModule {}
