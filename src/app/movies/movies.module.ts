import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MoviesRoutingModule } from './movies-routing.module';

import { MoviesComponent } from './containers/movies/movies.component';
import { DetailsComponent } from './containers/details/details.component';

import { LoaderComponent } from './components/loader/loader.component';
import { MovieComponent } from './components/movie/movie.component';
import { GenreFilterComponent } from './components/genre-filter/genre-filter.component';
import { SearchFormComponent } from './components/search-form/search-form.component';

import { PaginationPipe } from './pipes/pagination.pipe';

import { GenresService } from './services/genres/genres.service';
import { MoviesService } from './services/movies/movies.service';

@NgModule({
    imports: [
        BrowserAnimationsModule,
        CommonModule,
        FormsModule,
        MatPaginatorModule,
        MatSelectModule,
        MoviesRoutingModule,
        ReactiveFormsModule,
    ],
    declarations: [
        DetailsComponent,
        GenreFilterComponent,
        LoaderComponent,
        MoviesComponent,
        MovieComponent,
        PaginationPipe,
        SearchFormComponent,
    ],
    providers: [MoviesService, GenresService],
})
export class MoviesModule {}
