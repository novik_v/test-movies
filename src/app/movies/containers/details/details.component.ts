import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Movie } from '../../../shared/models/movie.model';
import { MoviesService } from '../../services/movies/movies.service';

@Component({
    selector: 'app-details',
    templateUrl: './details.component.html',
    styleUrls: ['./details.component.scss'],
})
export class DetailsComponent implements OnInit {
    movie: Movie;

    constructor(
        private _route: ActivatedRoute,
        private _moviesService: MoviesService,
    ) {}

    ngOnInit() {
        this._route.params.subscribe(params =>
            this._moviesService
                .getMovie(+params.id)
                .subscribe(data => (this.movie = data)),
        );
    }
}
