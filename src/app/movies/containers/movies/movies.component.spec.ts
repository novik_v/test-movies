import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';

import { GenreFilterComponent } from '../../components/genre-filter/genre-filter.component';
import { LoaderComponent } from '../../components/loader/loader.component';
import { MovieComponent } from './../../components/movie/movie.component';
import { MoviesComponent } from './movies.component';
import { PaginationPipe } from './../../pipes/pagination.pipe';
import { SearchFormComponent } from './../../components/search-form/search-form.component';

describe('MoviesComponent', () => {
    let component: MoviesComponent;
    let fixture: ComponentFixture<MoviesComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                GenreFilterComponent,
                LoaderComponent,
                MoviesComponent,
                MovieComponent,
                PaginationPipe,
                SearchFormComponent,
            ],
            imports: [
                BrowserAnimationsModule,
                MatSelectModule,
                MatPaginatorModule,
                ReactiveFormsModule,
                RouterTestingModule,
            ],
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MoviesComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
