import { Component, OnInit } from '@angular/core';

import { Movie } from '../../../shared/models/movie.model';
import { MoviesService } from '../../services/movies/movies.service';

@Component({
    selector: 'app-movies',
    templateUrl: './movies.component.html',
    styleUrls: ['./movies.component.scss'],
})
export class MoviesComponent implements OnInit {
    movies: Movie[];

    total: number;
    pageIndex = 0;
    pageSize = 10;

    selectedGenre: string;
    query: string;

    constructor(private _moviesService: MoviesService) {}

    ngOnInit() {
        this.getMovies();
    }

    onFilterByGenre(genre?: string): void {
        this.pageIndex = 0;
        this.getMovies(this.query, genre);
    }

    onSearch({ query }): void {
        this.pageIndex = 0;
        this.query = query;
        this.selectedGenre = '';
        this.getMovies(query);
    }

    goToPage({ pageSize, pageIndex }): void {
        this.pageIndex = pageIndex;
        this.pageSize = pageSize;
    }

    getMovies(query?: string, genre?: string): void {
        this._moviesService.getMovies({ query, genre }).subscribe(data => {
            this.movies = data;
            this.total = data.length;
        });
    }
}
