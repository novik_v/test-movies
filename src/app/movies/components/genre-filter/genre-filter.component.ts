import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

import { GenreType } from './../../../../../content/movie.model';
import { GenresService } from './../../services/genres/genres.service';

@Component({
    selector: 'app-genre-filter',
    templateUrl: './genre-filter.component.html',
    styleUrls: ['./genre-filter.component.scss'],
})
export class GenreFilterComponent implements OnInit {
    genres: GenreType[];

    @Input()
    selectedGenre: string;

    @Output()
    filter = new EventEmitter<GenreType>();

    constructor(private _genreServices: GenresService) {}

    ngOnInit() {
        this.genres = this._genreServices.getGenres();
    }

    onChange({ value }) {
        this.filter.emit(value);
    }
}
