import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'pagination',
})
export class PaginationPipe implements PipeTransform {
    transform(list: any, pageParams?: any): any {
        return list.slice(
            pageParams.number * pageParams.size,
            (pageParams.number + 1) * pageParams.size,
        );
    }
}
