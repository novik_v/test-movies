import { AppPage } from './app.po';
import { browser, ExpectedConditions } from 'protractor';

describe('workspace-project App', () => {
    let page: AppPage;

    beforeEach(() => {
        page = new AppPage();
    });

    it('should display list', () => {
        page.navigateTo();

        expect(page.getMovies().count()).toEqual(10);
    });

    it('should find movie', () => {
      page.navigateTo();
  
      page.getSearchInput().sendKeys('Gridiron Gang\n');
  
      expect(page.getMovies().count()).toEqual(1);
    });

    it('should display next page', () => {
        page.navigateTo();

        const prevPageButton = page.getPrevPageButton();

        expect(prevPageButton.isEnabled()).toBe(false);

        page.getNextPageButton()
            .click()
            .then(() => {
                expect(prevPageButton.isEnabled()).toBe(true);
            });
    });

    it('should redirect to movie and back', () => {
        page.navigateTo();

        page.getMovieLink()
            .click()
            .then(() => {
                expect(page.getBackButton().isDisplayed).toBeTruthy();
            })
            .then(() =>
                browser.wait(
                    ExpectedConditions.visibilityOf(page.getMovieName()),
                    1000,
                ),
            )
            .then(() => {
                expect(page.getMovieName().getText()).toContain('Deadpool');
            })
            .then(() => {
                page.getBackButton()
                    .click()
                    .then(() =>
                        expect(browser.getCurrentUrl()).not.toContain(
                            '/movies/1',
                        ),
                    );
            });
    });
});
