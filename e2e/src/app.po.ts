import { browser, by, element } from 'protractor';

export class AppPage {
    navigateTo() {
        return browser.get('/');
    }
    
    getSearchInput() {
        return element(by.css('app-search-form input'));
    }

    getMovieLink() {
        return element
            .all(by.css('.content-container li'))
            .get(0)
            .element(by.css('a'));
    }

    getMovieName() {
        return element(by.css('.movie__name'));
    }

    getBackButton() {
        return element(by.css('.back'));
    }

    getMovies() {
        return element.all(by.css('.content-container li'));
    }

    getNextPageButton() {
        return element(by.css('.mat-paginator-navigation-next'));
    }

    getPrevPageButton() {
        return element(by.css('.mat-paginator-navigation-previous'));
    }
}
